(ns mastermind.fonction)

(defn code-secret
  "tirage aleatoire de n pions de couleurs"
  [n]
  (loop [i 0
         v []]
    (if (< i n)
      (recur (inc i) (conj v (rand-nth (seq #{:red :blue :green :yellow :black :white}))))
      v)))


(defn vcontains
  "Si le vecteur v contient l'element elt"
  [v elt]
  (loop [v v
         bool false]
    (if (and (= bool false) (seq v))
      (recur (rest v) (if (= elt (first v))
                        true
                        false))
      bool)))

(defn indications
  "Indications des pions et du resultat de l'adversaire"
  [v1 v2]
  (loop [i 0
         v3 []]
    (if (>= i (count v2))
      v3
      (recur (inc i)  
      (if (= (nth v1 i) (nth v2 i))
        (conj v3 :magenta)
        (conj v3 (loop [j 0]
                   (if (>= j (count v2))
                     :black
                     (if (= (nth v2 i) (nth v1 j))
                       :white
                       (recur (inc j)))))))  ))))

(defn frequences
  "Retourne pour chaque element son nombre d'occurence dans le vecteur"
  [v]
  (loop [i 0
         mp {}]
    (if (>= i (count v))
      mp
      (recur (inc i) (if (contains? mp (nth v i))
                       (assoc mp (nth v i) (+ 1 (mp (nth v i))))
                       (assoc mp (nth v i) 1))))))


(defn filtre-indications
  "Corrige les indications obtenues a la q.2, en faisant correspondre le nombre d'occurence des pions"
  [v1 v2 vind]
  (loop [i 0
         freq (frequences v1)
         vres vind]
    (if (>= i (count v1))
      vres
      (recur (inc i) 
             (if (contains? freq (nth v2 i))
               (assoc freq (nth v2 i) (dec (get freq (nth v2 i))))
               freq) 
             (if (and  (contains? freq (nth v2 i)) (<= (freq (nth v2 i)) 0) (not= (nth vres i) :magenta))
               (assoc vres i :black)
               vres)))))

(defn entree-joueur
  "Le Joueur entre des couleurs"
  [n]
  (loop [i 0
         vres []]
    (if (< i n)
      (recur (inc i) (conj vres (keyword (str (read-line)))))
      vres)))

;;(entree-joueur 4)

(defn filtre-color
  "filtre l'ensemble de couleur par rapport au couleur dispo dans le code"
  [code]
  (loop [code code
         ens #{}]
    (if (seq code)
      (recur (rest code) (conj ens (first code)))
      ens)))

(defn decodeur
  "Essaie de corriger le code genere precedement (vres) a partir des indices donnes(vind)"
  [code vres vind]
  (loop [code2 code, vold vres, vind2 vind, vnew [], ens (filtre-color code)]
    (if (seq vind2)
      (if (= (first vind2) :magenta)
        (recur (rest code2) (rest vold) (rest vind2) (conj vnew (first vold)) ens)
        (recur (rest code2) (rest vold) (rest vind2) (conj vnew (rand-nth (seq ens))) ens))
      vnew)))