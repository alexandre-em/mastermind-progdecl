(ns mastermind.codecodeur
  (:require [mastermind.fonction :as g]
            [mastermind.inteface :as itrf]
            [lanterna.screen :as s]))

(defn joueur-codificateur
  "Lance une partie en tant que codificateur, la partie se termine lorsque le programme trouve le code ou lorsque 12 essais ont ete tente"
  [n]
  (println (str "Entrez le code secret (red/green/blue/yellow/black/white), taille =" n))
  (do (def scr (s/get-screen))
      (def x (first (s/get-size scr)))
      (def y (- (second (s/get-size scr)) 1))
      (s/start scr)
      (s/put-string scr 0 0 "Cote CODEUR")
      (s/put-string scr 10 (- (second (s/get-size scr)) 1) "CODE SECRET : ")
      (s/put-string scr (- (quot x 2) 12) 0 "CODE PROGRAMME   ")
      (s/put-string scr (- x (* 3 4)) 0 "INDICATION   "))
  (loop [vres (g/code-secret n), code (g/entree-joueur n) i 0]
    (do
      (itrf/cote-codeur-graph scr code x y)
      (s/put-string scr 10 (+ (* 2 i) 1) (str "essai " i))
      (itrf/cote-codeur-graph scr vres x (+ (* 2 i) 1)))
    (let [vind (g/filtre-indications code vres (g/indications code vres))]
      (do 
        (itrf/indic-graph scr vind x (+ (* 2 i) 1)))
      (if (and (or (g/vcontains vind :black) (g/vcontains vind :white)) (< i 12))
        (recur (g/decodeur code vres vind) code (inc i))
        (do
          (if (>= i 12)
            (s/put-string scr (quot x 2) (quot y 2) "CODE NON TROUVE")
            (s/put-string scr (quot x 2) (quot y 2) "CODE TROUVE"))
          (s/redraw scr)
          (Thread/sleep 3000)
          (s/stop scr)
          i)))))
